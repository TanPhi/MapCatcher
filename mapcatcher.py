#!/bin/sh
from xdo import Xdo
import time
import os
import math

import sys, getopt

def help():
    print 'Map Catcher'
    print 'options :'
    print '-h display this lines'
    print '-e estimation only'
    print '-w --win <str> browser window name [\'UT.no - Chromium\']'
    print '-b --boxes <int> number of boxes to explore along -y [3]'
    print '-s, --scale <int> map scale reglet [500]'
    print '-l, --law <int> reglet pixel number [103]'
    print '-o, --output <str> output name prefix [\'box\']'
    print '-x <int> capture rectangle offset x [160]'
    print '-y <int> capture rectangle offset y [225]'
    print '-k <int> key movement step in pixel [14]'
    print '-w <int> capture rectangle width steps [14]'
    print '-h <int> capture rectangle height steps [6]'
    print '-d <int> screenshot delay [2.5]'
    print '-p <int> keyboard delay [0.4]'
    print '\nNote: the map must be the last tab opened inside the browser'

def main(argv):
    inputfile = ''
    outputfile = ''
    try:
        opts, args = getopt.getopt(argv,"hqes:l:w:b:o:x:y:w:h:k:d:p:",["win=", "boxes=", "output=", "scale=","law=", "x=", "y=", "w=", "h=", "k=", "d=", "p="])
    except getopt.GetoptError:
        help()
        sys.exit(2)
        
    #default values
    scale = 500 #in meters
    pixel_law = 103 #in pixel
    win_name = 'UT.no - Chromium'
    output_prefix = 'box'
    x = 160
    y = 225
    w_key_offset = 14
    h_key_offset = 6
    x_key_offset = 80
    y_key_offset = 80
    delay_screenshot = 2.5
    delay_key = 0.4
    number_box_y = 3
    average_size = 7 #in Mo
    shutdown = False
    estimation_buffer = False
      
    for opt, arg in opts:
        if opt == '-h':
            help()
            sys.exit()
        elif opt == '-q':
            shutdown = True
        elif opt == '-e':
            estimation_buffer = True
        elif opt in ("-s", "--scale"):
            scale = int(arg)
        elif opt in ("-l", "--law"):
            pixel_law = int(arg)
        elif opt in ("-w", "--win"):
            win_name = arg
        elif opt in ("-b", "--boxes"):
            number_box_y = int(arg)
        elif opt in ("-o", "--output"):
            output_prefix = arg
        elif opt in ("-x", "--x"):
            x = int(arg)
        elif opt in ("-y", "--y"):
            y = int(arg)
        elif opt in ("-w", "--w"):
            w_key_offset = int(arg)
        elif opt in ("-h", "--h"):
            h_key_offset = int(arg)
        elif opt in ("-k", "--k"):
            x_key_offset = int(arg)
            y_key_offset = int(arg)
        elif opt in ("-d", "--d"):
            delay_screenshot = float(arg)
        elif opt in ("-p", "--p"):
            delay_key = float(arg)

    #catch parameters
    number_x = 3
    number_y = -3
    
    #screen parameters
    w = x_key_offset * w_key_offset
    h = y_key_offset * h_key_offset

    #precomputation
    estimation = (((delay_key * w_key_offset) + delay_screenshot) * math.fabs(number_x) + ((delay_key * h_key_offset) + delay_screenshot) * math.fabs(number_y) * math.fabs(number_x) + delay_screenshot) * math.fabs(number_box_y)
    print estimation
    distance_x = round(math.fabs(number_x)*w*scale/pixel_law*1e-3,2)
    distance_y = round(math.fabs(number_y)*h*scale/pixel_law*number_box_y*1e-3,2)
    surface = round(distance_x*distance_y, 2)
    
    print '\n-------------------------------------------\nMap Catcher\nAuthor: frm\nVersion: 0.1\n-------------------------------------------\n'
    
    print 'Time Estimation: '+str(math.floor(estimation/(3600))) +'h ' +str(math.floor(estimation%(3600)/60)) +'m '+ str(math.floor(estimation%60))+'s'
    print 'distance - x: '+str(distance_x)+' km'
    print 'distance - y: '+str(distance_y)+' km'
    print 'surface: '+str(surface)+' km^2'
    print 'total image size: '+str(round(average_size*number_box_y,2))+' Mo'
    
    if estimation_buffer==True:
        print '\nEstimation only: exit'
        sys.exit()
    
    if shutdown == True:
        print '\nThe system will shutdown when at the end of the process if this bash is in sudo mode'
    print '\nloading...'
    time.sleep(10)

    #start
    xdo = Xdo()

    win_id = xdo.search_windows(win_name)[0]

    xdo.move_mouse(400,400)
    xdo.activate_window(win_id)
    xdo.click_window(win_id, 1)

    time.sleep(delay_screenshot)

    for box_index in range(number_box_y):
        files_x = []
        for i in range(int(math.fabs(number_x))):
            files_y = []
            for j in range(int(math.fabs(number_y))):
                name_y = 'image_y'+str(j)+'.png'
                files_y.append(name_y)
                os.system('scrot '+name_y)
                os.system('convert -crop '+str(w)+'x'+str(h)+'+'+str(x)+'+'+str(y)+' '+name_y+' '+name_y) #crop
                
                if len(files_y)<math.fabs(number_y):
                    if number_y < 0:
                        key = 'Up'
                    else:
                        key = 'Down'
                    for k in range(h_key_offset):
                        xdo.send_keysequence_window(win_id, key)
                        time.sleep(delay_key)
                    time.sleep(delay_screenshot)
            
            if number_y < 0:
                files_y = reversed(files_y)

            files_y_list = ''
            for name_y in files_y:
                files_y_list += name_y + ' '
                
            name_x = 'image_x'+str(i)+'.png'
            files_x.append(name_x)
            query = 'convert -append '+files_y_list+' '+name_x
            os.system(query) #append
            
            number_y = -number_y

            if len(files_x) < math.fabs(number_x):
                if number_x < 0:
                    key = 'Left'
                else:
                    key = 'Right'
                for k in range(w_key_offset):
                    xdo.send_keysequence_window(win_id, key)
                    time.sleep(delay_key)
                time.sleep(delay_screenshot)

        if number_x < 0:
            files_x = reversed(files_x)

        files_x_list = ''
        for name in files_x:
            files_x_list += name + ' '
        name = output_prefix+str(box_index)+'.png'
        query = 'convert +append '+files_x_list+' '+name
        os.system(query) #append
        
        number_y = -number_y
        number_x = - number_x

    os.system('rm '+files_x_list+' '+files_y_list)
    
    if shutdown == True:
        time.sleep(60)
        os.system('shutdown -h now')

if __name__ == "__main__":
   main(sys.argv[1:])
