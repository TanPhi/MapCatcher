# Map Catcher

## Overview
MapCatcher is a bot designed to capture a large map from an online mapping service.
It is originally created for the Norwegian Hiking maps website www.ut.no/kart.
It could be easily adapted to an other map service if this one supports the keyboard movement control.

keywords : map, screenshot, stitch, bot, ut.no

## Requirements
- python library xdotool

## Using
Command to call the help:
> python mapcatcher.py -h